/*
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * DependencyVisualise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */

/**
 * This file defines a bunch of simple test packages and classes, used in various tests.
 */
package org.servicedev.ravioli.model.jar.sample {

import org.servicedev.ravioli.model.jar.sample.sub1.Sample1a

class Sample(s: Sample1a) {}

  package sub1 {

    class Sample1a {}

    class Sample1b {}
  }

  package sub2 {

    import org.servicedev.ravioli.model.jar.sample.sub1.Sample1b

    class Sample2a(s: Sample) {}

    class Sample2b(s: Sample1b) {}

    class Sample2c(s: Sample)
  }

  package sub3 {

    import org.servicedev.ravioli.model.jar.sample.sub1.Sample1b

    class Sample3a(s: Sample1a) {}

    class Sample3b(s: Sample1b) {}

    class Sample3c(s: Sample1a) {}
  }
}


