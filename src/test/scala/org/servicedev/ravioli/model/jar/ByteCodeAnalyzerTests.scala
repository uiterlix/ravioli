/*
 * Copyright 2013 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * DependencyVisualise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */

package org.servicedev.ravioli.model.jar

import org.scalatest.{Matchers, FunSuite}
import org.scalatest.mock.MockitoSugar
import org.servicedev.ravioli.model.jar.sample.Sample
import org.servicedev.ravioli.model.jar.sample.sub1.Sample1a
import org.servicedev.ravioli.model.jar.sample.sub2.{Sample2a, Sample2b, Sample2c}
import org.servicedev.ravioli.model.jar.sample.sub3._

class ByteCodeAnalyzerTests extends FunSuite with Matchers with MockitoSugar {

  val samplePackage = "org.servicedev.ravioli.model.jar.sample."

  test("Class using classes from package should result in dependent package") {
    val analyzer = new ByteCodeAnalyzer
    List(classOf[Sample]).foreach { clazz =>
      analyzer.extractTypeDependencies(TestClassReaderFactory.classReaderFor(clazz))
    }

    analyzer.dependentPackages("org.servicedev.ravioli.model.jar.sample") should contain ("org.servicedev.ravioli.model.jar.sample.sub1")
  }

  test("Several classes using class from other packages should result in dependent packages without duplicates") {
    val analyzer = new ByteCodeAnalyzer
    List(classOf[Sample2a], classOf[Sample2b], classOf[Sample2c], classOf[Sample1a]).foreach { clazz =>
      analyzer.extractTypeDependencies(TestClassReaderFactory.classReaderFor(clazz))
    }

    analyzer.dependentPackages("org.servicedev.ravioli.model.jar.sample.sub2") should contain allOf ("org.servicedev.ravioli.model.jar.sample", "org.servicedev.ravioli.model.jar.sample.sub1")
  }

  test("Package dependency should be based on used type") {
    val analyzer = new ByteCodeAnalyzer
    List(classOf[Sample2a], classOf[Sample2b], classOf[Sample2c], classOf[Sample1a]).foreach { clazz =>
      analyzer.extractTypeDependencies(TestClassReaderFactory.classReaderFor(clazz))
    }

    analyzer.usedTypes(samplePackage + "sub2", samplePackage + "sub1") should equal (Set(samplePackage + "sub1.Sample1b"))
  }

  test("When several classes from package use the sample classes, these should not be duplicated in the used types") {
    val analyzer = new ByteCodeAnalyzer
    List(classOf[Sample3a], classOf[Sample3b], classOf[Sample3c]).foreach { clazz =>
      analyzer.extractTypeDependencies(TestClassReaderFactory.classReaderFor(clazz))
    }

    analyzer.usedTypes(samplePackage + "sub3", samplePackage + "sub1") should equal (
      Set(samplePackage + "sub1.Sample1a", samplePackage + "sub1.Sample1b"))
  }

}
