/*
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * DependencyVisualise is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */

package org.servicedev.ravioli.model.jar

import org.servicedev.ravioli.model.{Dependency, Component}

/**
 * A {code Dependency} that is caused by the source component using Java types provided by the target.
 * @param sourceComponent
 * @param targetComponent
 * @param dependentTypes
 */
class JavaTypeDependency(sourceComponent: Component, targetComponent: Component, var dependentTypes: Set[String]) extends Dependency {

  def getSourceComponent = sourceComponent

  def getTargetComponent = targetComponent

//  def getPackages(): Set[String] = dependentPackages

  def getExplanation: Seq[String] = dependentTypes.toList.sortWith( _ < _)
}
