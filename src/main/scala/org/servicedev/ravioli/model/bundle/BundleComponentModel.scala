/**
 * Copyright 2013, 2014 ServiceDev team (see authors.txt)
 *
 * This file is part of Ravioli, an analysis and visualization tool
 * for OSGi components and their dependencies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * Ravioli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */
package org.servicedev.ravioli.model.bundle

import org.servicedev.ravioli.model.{Component, Dependency, ComponentModel}
import scala.collection.mutable
import org.servicedev.ravioli.Constants
import org.servicedev.ravioli.impl.graph.Progress
import scala.collection.JavaConversions._

/**
 * Base class for component models where the component represents an OSGi bundle.
 */
abstract class BundleComponentModel extends ComponentModel {

  protected def createComponents(allBundles: Seq[BundleMetaData], progressMonitor: Progress) = {

    var progress = 0
    if (progressMonitor != null) {
      progressMonitor.setRange(0, allBundles.length * 3)
      progressMonitor.setProgressText(s"Analyzing ${allBundles.length} bundles...")
    }

    def updateProgress() = if (progressMonitor != null) {
      progress = progress + 1
      progressMonitor.setCurrentProgress(progress)
      if (progressMonitor.cancelled()) throw new InterruptedException()
    }

    val bundleComponents = new mutable.HashMap[Long, BundleComponent]
    val bundleComponentsBySymbolicName = new mutable.HashMap[String, mutable.Set[BundleComponent]] with mutable.MultiMap[String, BundleComponent]
    val packageMapping = new mutable.HashMap[String, mutable.Set[BundleComponent]] with mutable.MultiMap[String, BundleComponent]

    // First pass: create mapping from package to exporting bundle
    allBundles.foreach { bundle =>
      updateProgress()
      val component = new BundleComponent(bundle)
      bundleComponents.put(bundle.bundleId, component)
      bundleComponentsBySymbolicName.addBinding(bundle.symbolicName, component)
      val exportHeader = bundle.getHeader("Export-Package")
      if (exportHeader != null)
        Manifest.parsePackageHeader(exportHeader).foreach { exportedPackage =>
          packageMapping.addBinding(exportedPackage, component)
        }
    }

    val uniquePackageMappings = packageMapping.filter { entry => entry._2.size == 1 }

    // Second pass: create dependencies for unique packages
    allBundles.foreach { bundle =>
      updateProgress()
      val currentComponent = bundleComponents(bundle.bundleId)
      val componentsProvidingOurPackages = new mutable.HashMap[BundleComponent, mutable.Set[String]] with mutable.MultiMap[BundleComponent, String]
      val imports = bundle.getHeader("Import-Package")
      if (imports != null)
        Manifest.parsePackageHeader(imports).map { importedPackage =>
          if (uniquePackageMappings.contains(importedPackage)) {
            val fullFillingComponent: BundleComponent = uniquePackageMappings(importedPackage).head
            // Ignore dependencies to self.
            if (fullFillingComponent != currentComponent)
              componentsProvidingOurPackages.addBinding(fullFillingComponent, importedPackage)
          }
        }

      componentsProvidingOurPackages.foreach { entry =>
        val target = entry._1
        val dependentPackages = entry._2.toSet   // make it immutable
      val newDependency = new PackageDependency(currentComponent, target, dependentPackages)
        currentComponent.addOutgoingDependency(newDependency)
      }
    }

    // Third pass: create all other dependencies, using the ones created already
    allBundles.foreach { bundle =>
      updateProgress()
      val currentComponent = bundleComponents(bundle.bundleId)
      val currentOutgoingDependencies: List[Dependency] = currentComponent.getOutgoingDependencies().toList
      val currentDependencyPackages: Set[String] =
        if (currentOutgoingDependencies.isEmpty)
          Set()
        else
          currentOutgoingDependencies.map { _.asInstanceOf[PackageDependency].getPackages() }.reduce { _ ++ _ }
      val imports = bundle.getHeader("Import-Package")
      if (imports != null)
        Manifest.parsePackageHeader(imports).map { importedPackage =>
        // First check whether this dependency is not yet fulfilled
          if (! currentDependencyPackages.contains(importedPackage)) {
            // Check if we've got a component that can fulfil
            if (packageMapping.contains(importedPackage)) {
              val dependencyFulfillingComponents = packageMapping(importedPackage)
              // If self fulfils the dependency, we don't need (or even want) to add a dependency
              if (! dependencyFulfillingComponents.contains(currentComponent)) {
                // First try if any of the existing related components can satisfy this dependency
                val relatedComponents: Set[BundleComponent] = currentOutgoingDependencies.map { _.getTargetComponent().asInstanceOf[BundleComponent] }.toSet
                val candidates = dependencyFulfillingComponents.intersect(relatedComponents)
                if (! candidates.isEmpty) {
                  // There is one, or more. Don't bother, just take the first.
                  val currentDependency = currentOutgoingDependencies.find { _.getTargetComponent() == candidates.head }.get
                  // Must update existing dependency: add package (used for explaination)
                  currentDependency.asInstanceOf[PackageDependency].addPackage(importedPackage)
                }
                else {
                  // None of the existing components fulfils, so we must create a new Dependency.
                  // Don't bother about selecting the most appropiate (yet), just take the first
                  val target = dependencyFulfillingComponents.head
                  val newDependency: PackageDependency = new PackageDependency(currentComponent, target, Set(importedPackage))
                  currentComponent.addOutgoingDependency(newDependency)
                }
              }
              // else: fulfilled by self, nothing to add
            }
            else {
              // Unsatisfied dependencies are currently simply ignored. TODO: add extra method to ComponentModel
              if (java.lang.Boolean.getBoolean(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY)) {
                System.out.println("Unsatisfied dependency for bundle " + bundle + ": " + importedPackage)
              }
            }
          }
        }
    }

    // Forth pass: extract require-bundle dependencies
    allBundles.foreach { bundle =>
      val requireBundleHeader = bundle.getHeader("Require-Bundle")
      if (requireBundleHeader != null)
        Manifest.parsePackageHeader(requireBundleHeader).foreach { requiredBundle =>
          // Get components that match the required bundle; can be more than one if multiple versions are deployed
          val requiredComponentCandidates = bundleComponentsBySymbolicName.get(requiredBundle)
          if (requiredComponentCandidates.isDefined) {
            // Just take the first
            val requiredComponent = requiredComponentCandidates.get.head
            // But if there are more, issue a warning
            if (requiredComponentCandidates.get.size > 1)
              println("Multiple bundles satisfy Require-Bundle " + bundle.symbolicName)

            val currentComponent = bundleComponents(bundle.bundleId)
            val requireBundleDependency = new BundleDependency(currentComponent, requiredComponent)
            currentComponent.addOutgoingDependency(requireBundleDependency)
          }
          else {
            // Unsatisfied dependencies are currently simply ignored. TODO: add extra method to ComponentModel
            if (java.lang.Boolean.getBoolean(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY)) {
              System.out.println("Unsatisfied require-bundle dependency for bundle " + bundle + ": " + requiredBundle)
            }
          }
        }
    }

    // Fifth pass: extract fragment bundle dependencies
    allBundles.foreach { bundle =>
      val fragmentHostHeader = bundle.getHeader("Fragment-Host")
      if (fragmentHostHeader != null)
        Manifest.parsePackageHeader(fragmentHostHeader).foreach { hostBundle =>
        // Get components that match the required bundle; can be more than one if multiple versions are deployed
          val candidates = bundleComponentsBySymbolicName.get(hostBundle)
          if (candidates.isDefined) {
            // Just take the first
            val hostComponent = candidates.get.head
            // But if there are more, issue a warning
            if (candidates.get.size > 1)
              println("Multiple bundles satisfy Fragment-Host " + bundle.symbolicName)

            val currentComponent = bundleComponents(bundle.bundleId)
            val fragmentHostDependency = new FragmentHostDependency(currentComponent, hostComponent)
            currentComponent.addOutgoingDependency(fragmentHostDependency)
          }
          else {
            // Unsatisfied dependencies are currently simply ignored. TODO: add extra method to ComponentModel
            if (java.lang.Boolean.getBoolean(Constants.DEPVIS_DEBUG_SYSTEM_PROPERTY)) {
              System.out.println("Unsatisfied fragment-host dependency for bundle " + bundle + ": " + hostBundle)
            }
          }
        }
    }

    bundleComponents.values.toList
  }

  override def explainDependencies(dependencies: Seq[Dependency]): String = {
    // Partition the dependencies by subtype
    val groupedDeps = dependencies.groupBy { _.getClass() }

    val collectedPackages = groupedDeps.getOrElse(classOf[PackageDependency], List()).map { _.asInstanceOf[PackageDependency].getExplanation }.flatten
    val packageDeps = if (! collectedPackages.isEmpty) "Package dependencies:<br/>" + collectedPackages.toSet.mkString("<br/>") else ""

    val requireBundleDeps = if (! groupedDeps.get(classOf[BundleDependency]).isEmpty)
      "Require Bundle dependencies: <br/>" + groupedDeps(classOf[BundleDependency]).map { _.asInstanceOf[BundleDependency].getExplanation }.toSet.mkString("<br/>")
    else ""

    val fragmentHostDeps = if (! groupedDeps.get(classOf[FragmentHostDependency]).isEmpty)
      "Fragment Host dependencies: <br/>" + groupedDeps(classOf[FragmentHostDependency]).map { _.asInstanceOf[FragmentHostDependency].getExplanation }.toSet.mkString("<br/>")
    else
      ""

    var explanation = packageDeps
    if (! explanation.isEmpty && !requireBundleDeps.isEmpty)
      explanation += "<br/>"

    explanation += requireBundleDeps

    if (! explanation.isEmpty && !fragmentHostDeps.isEmpty)
      explanation += "<br/>"

    explanation += fragmentHostDeps

    return s"<html>${explanation}</html>"
  }
}

trait BundleMetaData {
  def fileName: String
  def bundleId: Long
  def symbolicName: String
  def getHeader(name: String): String
}
