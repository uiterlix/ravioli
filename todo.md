This is the short term TODO list for the next release.
All long term TODO's should go on the [wiki](https://bitbucket.org/uiterlix/ravioli/wiki/Home).
Bugs that (for some strange reason) won't be fixed in the next release should go in the bug tracker.
This list should be empty when releasing a new version.

- explain shows doubles with grouped components
- what to do with unsatisfied dependencies? might be quite interesting for user (sort of external dependencies)

